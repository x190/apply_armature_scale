import bpy


def get_selected_objects(context, ob_types=None):
    selected = []
    for ob in context.view_layer.objects:
        if not ob.select_get() or ob.hide_get() or ob.hide_viewport:
            continue
        if (ob_types is not None) and (ob.type not in ob_types):
            continue
        selected.append(ob)
    return selected

def get_object_actions_names(ob, include_actions):
    assert( include_actions in {'ALL_NLA', 'CURRENT'} )
    actions_names = set()
    if ob.animation_data is not None:
        if ob.animation_data.action is not None:
            actions_names.add(ob.animation_data.action.name)
        if include_actions == 'ALL_NLA':
            for nla_track in ob.animation_data.nla_tracks:
                for s in nla_track.strips:
                    if s.action is not None:
                        actions_names.add(s.action.name)
    return actions_names

def get_actions(actions_names):
    actions = []
    for an in actions_names:
        acc = bpy.data.actions.get(an, None)
        if acc is not None:
            actions.append(acc)
    return actions


def is_not_one(s):
    return (abs(s-1) > 1e-6)

def is_scaled(ob):
    for s in ob.scale:
        if is_not_one(s):
            return True
    return False

def topmost_parent(ob):
    act = ob
    while act.parent is not None:
        act = act.parent
    return act

def parenting_order_objects(obs):
    obs_names = set([ob.name for ob in obs])

    topmost_parents = {}
    for ob in obs:
        tp = topmost_parent(ob)
        topmost_parents[tp.name] = tp

    ordered_obs = []
    for ob in topmost_parents.values():
        acts = [ob]
        while len(acts) > 0:
            next_acts = []
            for act in acts:
                if act.name in obs_names:
                    ordered_obs.append(act)
                next_acts.extend([e for e in act.children])
            acts = next_acts

    return ordered_obs

def get_tuple_str(t, decimals):
    assert(len(t) > 0)
    t_str = "("
    for e in t[:-1]:
        t_str += " {:.{}f},".format(e, decimals)
    t_str += " {:.{}f} )".format(t[-1], decimals)
    return t_str

def adjust_constraints(ob, target_scale):
    return

def scale_actions( ob, actions, target_scale, channels ):
    scaled_actions_names = set()
    inc_object_channels = channels in {'ALL', 'OBJECT'}
    inc_bones_channels = channels in {'ALL', 'BONES'}

    for acc in actions:
        for fcu in acc.fcurves:
            if not fcu.data_path.endswith('location'):
                continue
            if len(fcu.keyframe_points) == 0:
                continue
            is_object_channel = (fcu.data_path == 'location')
            if is_object_channel:
                if not inc_object_channels:
                    continue
            else:
                if not inc_bones_channels:
                    continue

            scale = target_scale[fcu.array_index]
            if not is_not_one(scale):
                continue

            scaled_actions_names.add( acc.name )

            for kfp in fcu.keyframe_points:
                kfp.co[1] *= scale
                kfp.handle_left[1] *= scale
                kfp.handle_right[1] *= scale

    adjust_constraints(ob, target_scale)
    return scaled_actions_names


def print_scaled_objects(d):
    if len(d) > 0:
        for ob_name, t in d.items():
            scale_str = get_tuple_str(t[1], decimals=5)
            if len(t[-1]) == 0:
                print("  \"{}\" (scale = {}; no actions).".format( ob_name, scale_str ) )
            else:
                print("  \"{}\" (scale = {}; {} actions):".format( ob_name, scale_str, len(t[-1]) ) )
                for acc_name in t[-1]:
                    print("    \"{}\"".format(acc_name))
            print()


def scale_objects_and_actions( context,
                               obs,
                               include_actions,
                               channels,
                               only_actions_scale,
                               silent=False ):

    ordered_obs = parenting_order_objects(obs)

    scaled_objects = {}
    already_scaled_acc_names = set()

    selected_actions = None
    if include_actions == 'CUSTOM':
        act_list = context.scene.apply_actions_scale_settings.action_list
        selected_actions = [e.name for e in act_list.entries if e.selected]

    for ob in ordered_obs:
        if only_actions_scale is None:
            if (not is_scaled(ob)):
                continue
            target_scale = tuple(ob.scale)
            bpy.ops.object.select_all(action='DESELECT')
            ob.select_set(True)
            bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
        else:
            target_scale = tuple(only_actions_scale)

        ob_scaled_info = ( ob.type == 'ARMATURE', target_scale, set() )

        ob_actions_names = None
        if selected_actions is None:
            ob_actions_names = get_object_actions_names(ob, include_actions)
        else:
            ob_actions_names = selected_actions
        ob_actions_names = [n for n in ob_actions_names if n not in already_scaled_acc_names]
        ob_actions = get_actions(ob_actions_names)
        already_scaled_acc_names.update(ob_actions_names)
        del ob_actions_names

        scaled_actions_names = scale_actions( ob, ob_actions, target_scale, channels )
        ob_scaled_info[-1].update( scaled_actions_names )
        if (only_actions_scale is None) or (len( ob_scaled_info[-1] ) > 0):
            scaled_objects[ob.name] = ob_scaled_info

    scaled_armatures = {}
    scaled_non_armatures = {}
    for ob_name, t in scaled_objects.items():
        if t[0]:
            scaled_armatures[ob_name] = t
        else:
            scaled_non_armatures[ob_name] = t

    n_armatures = len(scaled_armatures)
    n_others = len(scaled_non_armatures)

    if not silent:
        if n_armatures > 0:
            print("\n- Scaled armatures:")
            print_scaled_objects(scaled_armatures)
        if n_others > 0:
            print("\n- Scaled non-armature objects:")
            print_scaled_objects(scaled_non_armatures)

    return n_armatures, n_others
