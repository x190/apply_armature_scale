import bpy
from . import ops_scale

bl_info = {
    "name": "Apply actions scale",
    "author": "Xin",
    "version": (0, 2, 3),
    "blender": (2, 93, 0),
    "location": "3d view (Object mode) > ctrl + A > Apply actions' scale...",
    "description": "Apply scale to objects including their keyframed actions",
    "doc_url": "https://gitlab.com/x190/apply-actions-scale",
    "tracker_url": "https://gitlab.com/x190/apply-actions-scale",
    "category": "Animation",
}


class ActionListEntry(bpy.types.PropertyGroup):
    selected: bpy.props.BoolProperty(name="Selected", default=False)

class ActionList(bpy.types.PropertyGroup):
    index: bpy.props.IntProperty(default=-1, min=-1, name="Active action")
    entries: bpy.props.CollectionProperty(type=ActionListEntry)

class ApplyActionsScaleProperties(bpy.types.PropertyGroup):
    action_list:  bpy.props.PointerProperty(type=ActionList)


classes = (
    ActionListEntry,
    ActionList,
    ApplyActionsScaleProperties,

    ops_scale.ScaleObjectOperator,
    # ~ ops_scale.ScaleArmaturesMenu,
    ops_scale.CUSTOM_UL_actions,
)

def menu_func(self, context):
    layout = self.layout
    layout.separator()

    # ~ row = layout.row(align = True).split(factor=0.35)
    # ~ row.alignment = 'EXPAND'
    row = layout.row()
    row.operator_context = "INVOKE_DEFAULT"
    row.operator(ops_scale.ScaleObjectOperator.bl_idname, text="Apply actions' scale...")
    # ~ row.operator("wm.call_menu", text="Apply rig scale...").name=ops_scale.ScaleArmaturesMenu.bl_idname


def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.VIEW3D_MT_object_apply.append(menu_func)
    bpy.types.Scene.apply_actions_scale_settings = bpy.props.PointerProperty(type=ApplyActionsScaleProperties)

def unregister():
    del bpy.types.Scene.apply_actions_scale_settings
    bpy.types.VIEW3D_MT_object_apply.remove(menu_func)
    for c in reversed(classes):
        bpy.utils.unregister_class(c)

if __name__ == "__main__":
    register()
