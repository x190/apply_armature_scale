import bpy
from . import utils


class CUSTOM_UL_actions(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # ~ self.use_filter_show = False
        r = layout.row()
        split = r.split(factor=0.12)
        split.prop(item, "selected", text="", emboss=True, translate=False)
        split.label(text=item.name, translate=False)

class ScaleObjectOperator(bpy.types.Operator):
    """Apply objects scale (including their actions)"""
    bl_idname = "apply_armature_scale.scale_object"
    bl_label = "Apply Objects Scale"
    bl_options = {'UNDO'}

    include_actions: bpy.props.EnumProperty( name = "Include actions",
                                             items = ( ('CURRENT', "Only current", ""),
                                                       ('ALL_NLA', "All in nla", ""),
                                                       ('CUSTOM', "Custom", ""), ),
                                             default = 'ALL_NLA' )

    object_type: bpy.props.EnumProperty( name = "Objects type",
                                             items = ( ('ALL',  "All", "Scale selected meshes and armatures"),
                                                       ('ARMATURE', "Armatures", "Scale selected armatures"),
                                                      ),
                                             default = 'ALL',
                                             description= "Which selected objects to scale" )

    channels: bpy.props.EnumProperty( name = "Channels to scale",
                                             items = ( ('ALL',  "All", "Bones' and object's channels"),
                                                       ('BONES', "Only bones", "Only bones' channels"),
                                                       ('OBJECT', "Only object", "Only object's channels"), ),
                                             default = 'BONES',
                                             description= "Channels to scale when scaling actions" )

    scale_only_actions: bpy.props.BoolProperty(name="Scale only actions", default=False,
                            description="Only scale actions, not the objects")

    scale_factor: bpy.props.FloatProperty( name="Actions' scale", default = 1.0, min = 0,
                            description="Factor by which to scale actions" )

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        row = layout.row()
        row.prop(self, "include_actions")

        if self.include_actions == 'CUSTOM':
            act_list = context.scene.apply_actions_scale_settings.action_list
            row = layout.row()
            row.template_list( "CUSTOM_UL_actions", "",
                               act_list, "entries",
                               act_list, "index",
                               rows=2, maxrows=8 )

        row = layout.row()
        row.prop(self, "channels")
        row = layout.row()
        row.prop(self, "object_type")
        row = layout.row()
        row.prop(self, "scale_only_actions")
        if self.scale_only_actions:
            row = layout.row()
            row.prop(self, "scale_factor")

    def update_action_list(self, context):
        act_list = context.scene.apply_actions_scale_settings.action_list

        active_act = None
        if act_list.index >= 0 and act_list.index < len(act_list.entries):
            active_act = act_list.entries[act_list.index].name

        entries = []
        for a in bpy.data.actions:
            entry = act_list.entries.get(a.name, None)
            if entry is not None:
                selected = entry.selected
            else:
                selected = False
            entries.append((a.name, selected))

        act_list.entries.clear()
        act_list.index = -1

        for i, e in enumerate(entries):
            entry = act_list.entries.add()
            entry.name = e[0]
            if active_act is not None and entry.name == active_act:
                act_list.index = i
            entry.selected = e[1]

    def invoke(self, context, event):
        self.update_action_list(context)
        wm = context.window_manager
        return wm.invoke_props_dialog(self)  # width=600

    def execute(self, context):
        if not self.scale_objects(context):
            return {'CANCELLED'}
        return {'FINISHED'}

    def scale_objects(self, context):
        ob_types = None
        if self.object_type == 'ARMATURE':
            ob_types = set( ['ARMATURE'] )
        else:
            ob_types = set( ['MESH', 'ARMATURE'] )

        selected = utils.get_selected_objects( context, ob_types=ob_types )
        if len(selected) == 0:
            self.report({'ERROR'}, "No valid objects selected.")
            return False

        only_actions_scale = None
        if self.scale_only_actions:
            if not self.scale_factor > 0:
                self.report({'ERROR'}, "Zero scale")
                return False
            if not utils.is_not_one(self.scale_factor):
                self.report({'INFO'}, "No objects needed to be scaled.")
                return False
            only_actions_scale = (self.scale_factor,) * 3

        tot_scaled_armatures, tot_scaled_others = utils.scale_objects_and_actions(
                                                                       context,
                                                                       selected,
                                                                       self.include_actions,
                                                                       self.channels,
                                                                       only_actions_scale,
                                                                       silent=False )

        if tot_scaled_armatures == 0 and tot_scaled_others == 0:
            self.report({'INFO'}, "No objects needed to be scaled.")
            return False
        self.report({'INFO'}, "{} armatures, {} other objects scaled.".format(tot_scaled_armatures, tot_scaled_others))
        return True


# class ScaleArmaturesMenu(bpy.types.Menu):
#     bl_label = "Scale Armature"
#     bl_idname = "OBJECT_MT_scale_armatures_menu"
#
#     def draw(self, context):
#         layout = self.layout
#         layout.use_property_split = True
#         layout.use_property_decorate = False
#
#         row = layout.row()
#         row.operator(ScaleArmatureOperator.bl_idname)
